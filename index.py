from bottle import route, run, debug, static_file, redirect
from json import dumps, JSONEncoder
import tp3_marcos
import cPickle as pickle

debug(True)

class EquiposEncoder(JSONEncoder):
    def default(self, obj):
        #~ if not isinstance(obj, tp3_marcos.Equipo):
            #~ return super(EquiposEncoder, self).default(obj)

        if isinstance(obj, tp3_marcos.Equipo):
			combinedDict = obj.__dict__.copy()
			combinedDict.update({"ataque" : obj.ataque(), "defensa" : obj.defensa()})
			return combinedDict

        return obj.__dict__

# decorator
def json_result(f):
    def g(*a, **k):
        return dumps(f(*a, **k), cls=EquiposEncoder)
    return g

@route('/<page>')
@route('/')
def pages(page = ""):
	if "html" in page:
		return static_file(page, ".")
	elif page == "":
		return static_file("index.html", ".")
	else:
		redirect("/#/" + page)

@route('/api/equipo')
@route('/api/equipo/<nombre>')
@json_result
def api_equipo(nombre = None):
	equipos = tp3_marcos.obtener_equipos()
	if nombre is not None:
		return equipos[nombre]
	return equipos.values()

@route('/api/fixture')
@json_result
def api_fixture(nombre = None):
	return tp3_marcos.obtener_fixture()

@route('/api/simular/<id_partido:int>') # avanza un turno y devuelve el partido
@route('/api/simular/<id_partido:int>/<action>')
@json_result
def api_simular(id_partido, action = None):
	if action == "terminar":
		import os
		os.remove("partido" + str(id_partido) +".pickle")
		return
	try:
		with open("partido" + str(id_partido) +".pickle") as partido_file:
			sim = pickle.load(partido_file)
	except:
		partido = tp3_marcos.obtener_fixture()[id_partido]
		sim = tp3_marcos.SimuladorDePartidos(partido)

	sim.simular_turno()
	with open("partido" + str(id_partido) +".pickle", "w") as partido_file:
		pickle.dump(sim, partido_file)
	return sim.partido

run(reloader=True)
