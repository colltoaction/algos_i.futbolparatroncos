# encoding: utf-8
class Jugador(object):

    def __init__(self, nombre, posicion, \
            velocidad, resistencia, habilidad, precision):
        self.nombre = nombre
        self.pos = posicion
        self.vel = velocidad
        self.res = resistencia
        self.hab = habilidad
        self.prec = precision
    
    def nivel(self):
        return (self.vel+self.res+self.hab+self.prec) / 4

    def posicion(self):
        return self.pos

    def __str__(self):
        return '%s (%s) <%d,%d,%d,%d>' % (self.nombre,self.pos,self.vel,self.res,self.hab,self.prec)


class Equipo(object):
    def __init__(self, nombre):
        self.nombre = nombre
        self.plantel = {
                'ARQ': [], 
                'DEF': [],
                'VOL': [],
                'DEL': []
        }

    def agregar_jugador(self, jugador):
        self.plantel[jugador.posicion()].append(jugador)

    def defensa(self):
        jugadores = self.plantel['ARQ'] + self.plantel['DEF'] + self.plantel['VOL']
        return sum([j.nivel() for j in jugadores]) / len(jugadores)

    def ataque(self):
        jugadores = self.plantel['VOL'] + self.plantel['DEL']
        return sum([j.nivel() for j in jugadores]) / len(jugadores)

    def arquero(self):
        return self.plantel['ARQ'][0]

    def jugadores(self):
        return self.plantel['ARQ']+self.plantel['DEF']+self.plantel['VOL']+self.plantel['DEL']

    def __str__(self):
        return "%s (Ataque:%d, Defensa:%d)" % (self.nombre,self.defensa(),self.ataque())


class Partido(object):
    def __init__(self, local, visita):
        self.local = local
        self.visita = visita
        self.marcador = {local.nombre: 0, visita.nombre: 0}
        self.ultima_accion = ""

    def equipo_local(self):
        return self.local
    
    def equipo_visita(self):
        return self.visita

    def marcar_gol(self, equipo):
        self.marcador[equipo.nombre] += 1

    def resultado(self):
        return (self.marcador[self.local.nombre], self.marcador[self.visita.nombre])

    def __str__(self):
        local = self.local.nombre
        visita = self.visita.nombre
        return "%s (%d) vs (%d) %s" % (local, self.marcador[local], self.marcador[visita], visita)



import random

class SimuladorDePartidos(object):

    def __init__(self, \
            partido, \
            peso_nivel_pateador=0.3, \
            probabilidad_por_posicion={'ARQ':5,'DEF':20,'VOL':40,'DEL':50}):

        self.prob_por_posicion = probabilidad_por_posicion
        self.peso_nivel_pateador = peso_nivel_pateador
        self.partido = partido

        # El equipo local siempre comienza atacando
        self.equipo_atacante = partido.equipo_local()
        self.equipo_defensor = partido.equipo_visita()
    
    def simular_turno(self):
        aa = self.equipo_atacante.ataque()
        dd = self.equipo_defensor.defensa()

        if self.mantiene_posesion(aa,dd): 
            # equipo_atacante mantiene la posesion
            
            if self.decide_tirar_al_arco(aa):
                pateador = self.seleccionar_pateador(self.equipo_atacante.jugadores())
                arquero = self.equipo_defensor.arquero()

                if self.convierte_gol(pateador, arquero):
                    # equipo_atacante convierte un gol
                    self.partido.ultima_accion = "%s de %s se dispone a tirar al arco y ¡GOL!" % (pateador.nombre, self.equipo_atacante.nombre)
                    self.partido.marcar_gol(self.equipo_atacante)
                else:
                    # Falla el tiro, cambio de posesion
                    self.partido.ultima_accion = "%s de %s intenta un chutazo, pero el arquero %s ataja limpiamente" % (pateador.nombre, self.equipo_atacante.nombre, arquero.nombre)
                    self.equipo_atacante, self.equipo_defensor = self.equipo_defensor, self.equipo_atacante
            else:
                self.partido.ultima_accion = "%s pasa la pelota limpiamente" % (self.equipo_atacante.nombre)
                 

        else:   
            # equipo_atacante pierde la posesion
            self.partido.ultima_accion = "En una gran maniobra, %s roba el balón" % self.equipo_defensor.nombre
            self.equipo_atacante, self.equipo_defensor = self.equipo_defensor, self.equipo_atacante

    def mantiene_posesion(self, aa, dd):
        return self.simular(1,int(aa+(dd/2)),aa)

    def decide_tirar_al_arco(self, aa):
        return self.simular(1,100,aa)

    def convierte_gol(self,pateador,arquero):
        prob_nivel = pateador.nivel() * self.peso_nivel_pateador
        prob_posicion = self.prob_por_posicion[pateador.posicion()] * (1-self.peso_nivel_pateador)

        prob_tiro_al_arco = (prob_nivel+prob_posicion)/2
        prob_atajar = arquero.nivel()

        return self.simular(1,100,prob_tiro_al_arco) \
                and not self.simular(1,100,prob_atajar / 2)

    def seleccionar_pateador(self, jugadores):
        return random.choice(jugadores)

    def simular(self, a, b, c):
        return random.randint(a,b) < c


def generar_archivo_aleatorio():
    f=open('jugadores.csv', 'w')
    ri=random.randint
    pos=('ARQ','DEF','VOL','DEL')
    for i in range(1,5):
        for j in range(0,4):
            f.write('Equipo%d,Jugador%d%d,%s,%d,%d,%d,%d\n' \
                    % (i,i,j,pos[j],ri(0,99),ri(0,99),ri(0,99),ri(0,99)))
    f.close()


# Lectura del archivo de jugadores
def leer_archivo_jugadores():
    f = open('jugadores.csv')
    linea = f.readline()
    equipos = {}
    while linea:
        linea = linea.strip('\n')
        eq,jug,pos,v,r,h,p = linea.split(',')
        if not eq in equipos:
            equipos[eq] = Equipo(eq)
        jugador = Jugador(jug,pos,int(v),int(r),int(h),int(p))
        equipos[eq].agregar_jugador(jugador)
        linea = f.readline()
    f.close()
    return equipos

# Lectura del archivo de fixture
def leer_archivo_fixture():
    equipos = obtener_equipos()
    f = open('fixture.csv')
    linea = f.readline()
    partidos = []
    while linea:
        linea = linea.strip('\n')
        eq1,eq2 = linea.split(',')
        equipo1 = equipos[eq1]
        equipo2 = equipos[eq2]
        partidos.append( Partido(equipo1,equipo2) )
        linea = f.readline()
    f.close()
    return partidos

# Escritura del archivo de resultados
def escribir_resultados():
    f = open('resultados.csv', 'w')
    sim = SimuladorDePartidos()
    for partido in partidos:
        sim.simular_partido(partido)
        f.write(str(partido)+'\n')
    f.close()

def obtener_equipos():
    return leer_archivo_jugadores()

def obtener_fixture():
    return leer_archivo_fixture()
